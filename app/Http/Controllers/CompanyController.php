<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Company;
use \Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        
        $firstName  = $request->firstname;
        $lastname   = $request->lastname;
        $email      = $request->email;
        $zipcode    = $request->zipcode;
        $phone      = $request->phone;
        
        $store = Company::where('zipcode', '=', $zipcode)->firstOrFail();
        
        $json = response()->json([
            'firstName' => $firstName,
            'lastname'  => $lastname,
            'email'     => $email,
            'zipcode'   => $zipcode,
            'phone'     => $phone,
            'storeID'   => $store->storeId
        ]);
        
        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://coregistros.free.beeceptor.com/my/api/path', [
            'form_params' => [
                'data' => $json
            ]
        ]);
        
        
        return $response->getBody()->getContents();
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
