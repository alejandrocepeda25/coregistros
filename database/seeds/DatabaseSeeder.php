<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    
        DB::table('companies')->insert([
        	'id'			=> 1,
            'name' 			=> 'Company 1',
            'zipcode' 	    => '08456',
            'storeId' 	    => 'CO1234'
        ]);

        DB::table('companies')->insert([
        	'id'			=> 2,
            'name' 			=> 'Company 2',
            'zipcode' 	    => '09789',
            'storeId' 	    => 'CO5678'
        ]);

        DB::table('companies')->insert([
        	'id'			=> 3,
            'name' 			=> 'Company 3',
            'zipcode' 	    => '04536',
            'storeId' 	    => 'ES3456'
        ]);
    }
}
